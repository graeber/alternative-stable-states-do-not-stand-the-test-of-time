# libraries ----
library(tidyverse)
library(foreach)
library(doParallel)
library(diptest)

# folders ----
exportfolder <- c("C:/INSERT/DATAFOLDER") # define folder for export


# global vars ----
# Iterations for bootstrap resampling and plotting
its <- 500

# Cores for parallel processing
cores <- 12

# Number of gap years
g <- 2 #years

# register cores -----
cl <- makePSOCKcluster(cores)
registerDoParallel(cl)

# Hierarchical bootstrap -----
# Simplified version for simulation data
# See also separate R file on resampling for detailled comments
hier_resampl <- function(ass = sim_data, n_means = 5, b = 300) {
  
  smpl_b <- foreach(i = 1:b, .combine = c, .packages = c("dplyr", "diptest"), .verbose = FALSE) %dopar% {
    
    site_vec_b <- sample(unique(ass$lake), length(unique(ass$lake)), replace = TRUE) # random lake vector (sampling with replacement)
    
    smpl_n <- NULL # Empty vector to fill
    for(j in 1:length(site_vec_b)) {
      sub <- ass %>% filter(lake == site_vec_b[j]) # Select first lake of random dataset
      
      if(n_means > 1) { # if a mean of two or more observations from one lake should be created
        # select the lake from the vector of consecutive data
        s_vec <- which(1:nrow(sub) >= n_means) # Check which part of the data is longer than the requested average length
        start_sample <- s_vec[sample.int(length(s_vec),1)] # ** third-level resampling without replacement ** of end year of mean sample within group with sufficient years
        
        # select the previous years for mean calculation
        sub2 <- sub[(start_sample - (n_means-1)) : start_sample,]
        
        # generate iteration output
        sub_n2 <- sub2 %>% dplyr::select(x, y)
        
        # Calculate mean for selected data 
        fin <- apply(sub_n2, 2, mean) %>% t() # mean calculation and transposition
        fin2 <- data.frame(lake = sub2$lake[1], x = fin[,1], y = fin[,2]) # combine with lake identity
        fin <- fin2 # rename
      } else { # if only a single observation (year) should be extracted
        rmn <- sample(1:nrow(sub), 1, replace = FALSE) # select a single value
        sub2 <- sub[rmn,] # extract single value from data
        
        fin <- sub2 # rename
      }
      smpl_n <- rbind(smpl_n, fin) # combine fin (iteration output for one lake) with overall dataset from this iteration
      
    }
    smpl_n <- as_tibble(smpl_n) # make a Tibble
    
    # Check, if GLM converges
    options(show.error.messages = FALSE) 
    try_y_x <- try(glm(y ~ x, data = smpl_n, family = Gamma(link = "identity")))
    options(show.error.messages = TRUE)
    
    # IF GLM converges, do the following
    if("try-error" %in% class(try_y_x) == FALSE) {
      lm_y_x <- glm(y ~ x, data = smpl_n, family = Gamma(link = "identity")) # make GLM
      
      loop_list <- list() # create list
      loop_list$smpl <- smpl_n # add values from this iteration to a list
      loop_list$dip <- dip.test(x = smpl_n$y)$p.value # add p value from HD test of this iteration to a list
      loop_list$y_x <- lm_y_x # add GLM model to list
      
      loop_list # return list
    }
  }
  return(smpl_b) # return total list based on lists from iterations, this is the final function output
}


# Scatter plot function -----
# This is the functions used for scatter plots in the document
# combined lm and scatter plot function
# shp = point shape
# mains = optional title text for plot
# sze = point size
# p_type = type of plot, either from single years (s) or mean years (m), for use of different point color dependent on the type of plotted data

ass_scatter_lm <- function(lt_data, mains = "",
                           shp = 19, sze = 1, p_type = "s") {
  if(p_type == "s") paletti <- "#00afbb1a" # color for dots from single years
  if(p_type == "m") paletti <- "#c3ad2c1a" # color for dots from mean years
  
  lm_paletti <- "#79137053" # color of linear model estimate line
  
  if(length(lt_data) > (3*40)) {
    lt_data <- lt_data[1:(3*40)]
  }
  
  ylabs <- "x"
  xlabs <- "y"
  
  # Extract data for scatter plot
  smpls <- lt_data[which(names(lt_data) == "smpl")]
  xs <- smpls %>% sapply( "[[", "x")
  ys <- smpls %>% sapply( "[[", "y")
  
  # Extract data for lm plot
  d_lm <- lt_data[which(names(lt_data) == "y_x")]
  d_model <- d_lm %>% sapply( "[[", "model")
  
  # prep scatter data
  xy <- data.frame(xs = xs[,1], ys = ys[,1])
  
  # prep lm plot data
  lm_x <- xs[,1]
  lm_y <- predict.lm(d_lm[[1]])
  lm_xy <- data.frame(lmx = lm_x, lmy = lm_y)
  
  # plot
  xy_plot <- xy %>% ggscatter(x = "xs", y = "ys", color = paletti, shape = shp, size = sze, xlab = xlabs, ylab = ylabs) + 
    geom_line(data = lm_xy, aes(x = lmx, y = lmy), color = lm_paletti)
  
  for(i in 2:ncol(xs)) {
    # prep scatter data
    xy <- data.frame(xs = xs[,i], ys = ys[,i])
    
    # prep lm plot data
    lm_x <- xs[,i]
    lm_y <- predict.lm(d_lm[[i]])
    lm_xy <- data.frame(lmx = lm_x, lmy = lm_y)
    
    # plot
    xy_plot <- xy_plot + geom_point(data = xy, aes(x = xs, y = ys), color = paletti, shape = shp, size = sze) + geom_line(data = lm_xy, aes(x = lmx, y = lmy), color = lm_paletti)
  }
  
  return(xy_plot %>% ggpar(xlab = "x", ylab = "y", main = mains))
}


# Combined proxy plot of the simulations -----
# This is the function used for proxy plots in the document.
# m1 = first choice of model (single year used in text)
# m3_m = second choice of model (three years years used in text)
# m7_m = third choice of model (5 years years used in text)
# mode = should a mode of the distribution be plotted?
# modeltype = What is the name of the model in the lists? This name was defined in the bootstrap function above.
# variable = Which variable should be plotted?:
  # intercept = Model intercept
  # slope = Model slope
  # r2 = model pseudo R²
  # residuals = Model residuals
  # dip = p value of HD / dip test
  # density = Kernel density plot
# mains = optional title text for plot
# x_axis = label for x axis
# vert_line = an optional vertical line

combi_sim_plot <- function(m1, m3_m, m7_m, mode = TRUE, modeltype = "y_x", variable = c("intercept", "slope", "r2", "residuals", "fitted", "dip", "density"), mains = "", x_axis = "", vert_line = NULL) {
  
  paletti_s_fill <- "#00afbb7f"
  paletti_m_fill <- "#c3ad2c7f"
  
  paletti_s <- "#00afbbff"
  paletti_m <- "#c3ad2cff"
  
  plot_list <- list()
  
  lt_data <- m1
  d_lm <- lt_data[which(names(lt_data) == modeltype)]
  d_model <- d_lm %>% sapply( "[[", "model")
  r2 <- NULL; resis <- NULL;
  for(i in 1:ncol(d_model)) {
    r2 <- c(r2, 1 - (d_lm[[i]]$deviance / d_lm[[i]]$null.deviance)) # McFadden's pseudo r-squared 
    resis <- c(resis, residuals(d_lm[[i]], type = "response"))
  }
  plot_list$r2_1 <- r2
  plot_list$dips_1 <- lt_data[which(names(lt_data) == "dip")] %>% unlist()
  plot_list$resis_1 <- resis
  
  smpls <- lt_data[which(names(lt_data) == "smpl")]
  plot_list$ys_1 <- smpls %>% sapply( "[[", "y") %>% as.vector()
  
  lt_data <- m7_m
  d_lm <- lt_data[which(names(lt_data) == modeltype)]
  d_model <- d_lm %>% sapply( "[[", "model")
  r2 <- NULL; resis <- NULL;
  for(i in 1:ncol(d_model)) {
    r2 <- c(r2, 1 - (d_lm[[i]]$deviance / d_lm[[i]]$null.deviance)) # McFadden's pseudo r-squared 
    resis <- c(resis, residuals(d_lm[[i]], type = "response"))
  }
  plot_list$r2_m_7 <- r2
  plot_list$dips_m_7 <- lt_data[which(names(lt_data) == "dip")] %>% unlist()
  plot_list$resis_m_7 <- resis
  
  smpls <- lt_data[which(names(lt_data) == "smpl")]
  plot_list$ys_m_7 <- smpls %>% sapply( "[[", "y") %>% as.vector()
  
  lt_data <- m3_m
  d_lm <- lt_data[which(names(lt_data) == modeltype)]
  d_model <- d_lm %>% sapply( "[[", "model")
  r2 <- NULL; resis <- NULL; 
  for(i in 1:ncol(d_model)) {
    r2 <- c(r2, 1 - (d_lm[[i]]$deviance / d_lm[[i]]$null.deviance)) # McFadden's pseudo r-squared 
    resis <- c(resis, residuals(d_lm[[i]], type = "response"))
  }
  
  plot_list$r2_m_3 <- r2
  plot_list$dips_m_3 <- lt_data[which(names(lt_data) == "dip")] %>% unlist()
  plot_list$resis_m_3 <- resis
  
  smpls <- lt_data[which(names(lt_data) == "smpl")]
  plot_list$ys_m_3 <- smpls %>% sapply( "[[", "y") %>% as.vector()
  
  
  boone_translator <- tibble(funct_name = c("intercept", "slope", "r2", "residuals", "fitted", "HD", "density"),
                             data_name = c("cept", "slopi", "r2", "resis", "fittis", "dips", "ys"))
  
  useme <- boone_translator %>% filter(funct_name == variable) %>% .$data_name
  
  var_list <- plot_list %>% .[grep(useme, names(.))]
  
  cepter <- bind_rows(tibble(Type = "Single", Year = "1-year", y = var_list %>% .[grep("_1", names(.))] %>% unlist()),
                      tibble(Type = "Mean", Year = "3-year", y = var_list %>% .[grep("m_3", names(.))] %>% unlist()),
                      tibble(Type = "Mean", Year = "5-year", y = var_list %>% .[grep("m_7", names(.))] %>% unlist())
  )
  
  
  lower <- function(x) hdi(x)[1]
  upper <- function(x) hdi(x)[2]
  
  heidi <- cepter %>% 
    group_by(Year, Type) %>% 
    summarise(lower = lower(y), upper = upper(y), mode = mlv(y, method = "meanshift")[1], .groups = "keep")
  
  if(variable == "density") {
    g_cepter <- cepter %>% ggplot(aes(x = y, y = Year, fill = Type)) +
      #geom_density_ridges(alpha = 0.4, scale = 0.8, from = 0, color = "darkgreen", fill = NA) +
      stat_density_ridges(geom = "density_ridges", alpha = 0.4, bandwidth = 10, #bw.SJ(y), 
                          scale = 0.8, from = 0, fill = NA, color = "darkgreen") +
      stat_density_ridges(geom = "density_ridges", alpha = 0.4, bandwidth = 50, #bw.SJ(y), 
                          scale = 0.8, from = 0) +
      geom_errorbarh(inherit.aes = FALSE, data = heidi, 
                     aes(xmin = lower, xmax = upper, y = Year, color = Type), 
                     height = 0.1, show.legend = FALSE, position = position_dodge(width = 0.1)) +
      scale_color_manual(values=c(paletti_m, paletti_s)) +
      scale_fill_manual(values=c(paletti_m_fill, paletti_s_fill))
  } else {
    if(variable == "r2") {
      cepter2 <- cepter %>% filter(y >= 0, y <= 1)
      g_cepter <- cepter2 %>% ggplot(aes(x = y, y = Year)) +
        geom_density_ridges(aes(fill = Type), alpha = 0.4, scale = 0.8, from = 0, to = 1) +
        #stat_density_ridges(geom = "density_ridges", alpha = 0.4, bandwidth = bw.SJ(y, lower = 0.001, upper = 0.1), scale = 0.8, from = 0, to = 1) +
        geom_errorbarh(inherit.aes = FALSE, data = heidi, 
                       aes(xmin = lower, xmax = upper, y = Year, color = Type), 
                       height = 0.1, show.legend = FALSE, position = position_dodge(width = 0.1)) +
        scale_color_manual(values=c(paletti_m, paletti_s)) +
        scale_fill_manual(values=c(paletti_m_fill, paletti_s_fill))
    } else {
      g_cepter <- cepter %>% ggplot(aes(x = y, y = Year)) +
        geom_density_ridges(aes(fill = Type), alpha = 0.4, scale = 0.8) +
        #stat_density_ridges(geom = "density_ridges", alpha = 0.4, bandwidth = bw.SJ(y), scale = 0.8) +
        geom_errorbarh(inherit.aes = FALSE, data = heidi, 
                       aes(xmin = lower, xmax = upper, y = Year, color = Type), 
                       height = 0.1, show.legend = FALSE, position = position_dodge(width = 0.1)) +
        scale_color_manual(values=c(paletti_m, paletti_s)) +
        scale_fill_manual(values=c(paletti_m_fill, paletti_s_fill))
    }
  }
  
  if(mode == TRUE) {
    g_cepter <- g_cepter + geom_point(inherit.aes = FALSE, data = heidi, 
                                      aes(x = mode, y = Year, color = Type), position = position_dodge(width = 0.1))
  }
  
  
  if(is.null(vert_line) == FALSE) {
    g_cepter <- g_cepter + geom_vline(xintercept = vert_line)
  }
  
  
  return(g_cepter %>% ggpar(main = mains, xlab = x_axis))
}


# set exportfolder and export variables -----
setwd(exportfolder) # set export folder
wd = 5.5 # width of plot to be exported
hgt = 9 # height of plot to be exported


# Simulations from SI ---------------------------------------
# See SI for details on the simulations. 

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## 1. Scenario without stable alternative states -----

# set number of lakes 
lakes <- 99  # number of lakes, based on number of lakes in data (5-year means)
samples <- 20 # based on max range of sampled years per lake in data (6-year means)

# set mean x (nutrient concentration) for each lake, based on mean range in data (6-year means)


# to get repeatable results of the simulations, seeds are used (taken from good movies / literature)
set.seed(2001) # use same seed to get replicable answer, here a space odyssey (random number seed for science fiction lovers)
seqs <- sample(seq(0.33, 4.93, length.out = lakes)) # mean range based on total nitrogen range in data (with higher shape variable (below), this also works for total phosphorus)

# Shape variable of Gamma distribution for each of the two alternative stable states, higher values indicate less variability
# Shape variable equals shape variable of true distribution of data (true value is 2.63 ± 0.08 SE (GLM with TN+TP as explanatory variables))
y_shape <- 2.63


# Loop to create simulated dataset
lake <- NULL; y <- NULL; x <- NULL; type <- NULL
for(i in 1:lakes) {
  sl_a <- 40 # slope of simulated data
  ic_a <- 0 # intercept of simulated data
  
  #s <- sample.int(samples) # shuffling of years
  set.seed(i+1984)  # another seed for replicable random numbers, here this is added to i to get different seeds for each iteration (otherwise the simulations would not differ between iterations) this time from George Orwell
  loop_x <- abs(rnorm(n = samples, mean = seqs[i], sd = seqs[i]*0.35)) # random sample for 20 lakes based on normal distribution for mean range based on true range and cv * mean, as cv is stable over data mean range, with reduced SD uncertainty, as a higher SD would result in x values partially out of the range of the real world data, abs = absolute values to 
 
  x_a <- loop_x # rename x
  
  y_a_t <- sl_a * loop_x + ic_a # make linear model of relationship between x and y
  
  set.seed(i+42) # # another seed for replicable random numbers, here this is added to i to get different seeds for each iteration (otherwise the random scatter of simulated relationship would not differ between iterations)  Don’t Panic. “What is the meaning of life, the universe, and everything?” Guess who is this?
  y <- c(y, rgamma(samples, rate = y_shape / y_a_t, shape = y_shape)) # add random scatter based on Gamma distribution to model
  
  x <- c(x, x_a) # combine x with previous x
  
  # add lake id
  lake <- c(lake, 
            rep(i,length(x_a)))
}


sim_data <- data.frame(lake, x, y)  # make a data frame

sim_data %>% ggscatter(y = "y", x = "x", main = "Full simulated data") # check simulation result with a simple scatter plot

# bootstrap resampling of simulated data
chk_1 <- hier_resampl(n_means = 1, b = its) # for 1 year
chk_3 <- hier_resampl(n_means = 3, b = its) # for 3 years
chk_5 <- hier_resampl(n_means = 5, b = its) # for 5 years

# make scatter plots
sc1 <- ass_scatter_lm(lt_data = chk_1, mains = "1 year", p_type = "s")
sc2 <- ass_scatter_lm(lt_data = chk_3, mains = "3-mean years", p_type = "m")
sc3 <- ass_scatter_lm(lt_data = chk_5, mains = "5-mean years", p_type = "m")

# make proxy plots
combi1 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "r2", x_axis = "R²") %>% 
  ggpar(xlim = c(0,1))
combi2 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "residuals", x_axis = "GLM response residuals", mode = TRUE)
combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "density", x_axis = "Chl-a, kernel density\nhigh- (50, filled) &\nlow-bandwith (10, lines)", vert_line = NULL) # %>% ggpar(xlim = c(0,1))

# combine scatter and proxy plots
dragon <- (sc3 + combi1) / (sc2 + combi2) / (sc1 + combi3) + plot_layout(guides = "collect")
dragon

# export plots
ggsave(filename = "proxy noASS.svg", plot = dragon, height = hgt, width = wd)

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## 2. Scenario few years with different response in same lake, unstable alternative states (randomly single/ few years fall in different state) -----
# See 1. SI scenario for more comments, especially on the plotting and plot export

lakes <- 99  # number of lakes, based on number of lakes in data (5-year means)
samples <- 20 # number lake years min to max, based on max. range of sampled years per lake in data (6-year means)

alternative_state <- 0.50 # share of lakes with alternative stable state, hence alternative state is ocurring after half of the time. Alternative-stable state occurence does not matter here, because data is randomized to generate unstable states

# set mean x (nutrient concentration) for each lake, based on mean range in data (6-year means)
set.seed(2001) # a space odyssey
seqs <- sample(seq(0.33, 4.93, length.out = lakes)) # random sample for 20 lakes based on normal distribution for mean range based on true range and cv * mean, as cv is stable over data mean range, with reduced SD uncertainty, as a higher SD would result in x values partially out of the range of the real world data, abs = absolute values to 

# Variability of the Gamma distribution, with a shape of 15 for each of the different state populations, one gets a simulation output of approx 2.63 for the entire simulation dataset, which is close to the real data
y_shape <- 15

lake <- NULL; y <- NULL; x <- NULL; type <- NULL
for(i in 1:lakes) {
  # Population in state a
  sl_a <- 120 # slope
  ic_a <- 50 # intercept
  
  # Population in state b
  sl_b <- 40 # slope
  ic_b <- 0 # intercept
  
  set.seed(i+1984)  # George Orwell
  s <- sample.int(round(samples * alternative_state,0)) # random integer for sampling from the two populations
  
  rs <- sample.int(samples) # shuffling of years
  
  set.seed(i+1984)  # George Orwell
  loop_x <- abs(rnorm(n = samples, mean = seqs[i], sd = seqs[i]*0.35)) # mean range based on true range and cv * mean, as cv is stable over data mean range, with reduced SD uncertainty, as a higher SD would result in x values partially out of the range of the real world data
  
  x_a <- loop_x[-s] # random sample from population a, all samples which were NOT sampled for population a
  x_b <- loop_x[s] # random sample from population b
  
  # generation of linear models
  y_a_t <- sl_a * loop_x[-s] + ic_a
  y_b_t <- sl_b * loop_x[s] + ic_b 
  
  set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
  l1 <- rgamma(samples - round(samples * alternative_state, 0), rate = y_shape / y_a_t, shape = y_shape)
  set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
  l2 <- rgamma(round(samples * alternative_state, 0), rate = y_shape / y_b_t, shape = y_shape)
  
  # Shuffling of years to generate unstable alternative states
  # all variables are shuffled based on the same variable "rs", hence are shuffled in the same way
  # y shuffle
  y <- c(y, c(l1, l2)[rs])
  
  # x shuffle
  x <- c(x,
         c(x_a, x_b)[rs]
  )
  
  # population descriptor shuffle
  type <- c(type, 
            c(rep("a", length(x_a)), rep("b", length(x_b)))[rs]
  )
  
  # lake id shuffle
  lake <- c(lake, 
            rep(i,length(c(x_a, x_b)))
  )
  
}

sim_data <- data.frame(lake, type, x, y)

sim_data %>% ggscatter(y = "y", x = "x", color = "type", main = "Full simulated data")

chk_1 <- hier_resampl(n_means = 1, b = its)
chk_3 <- hier_resampl(n_means = 3, b = its)
chk_5 <- hier_resampl(n_means = 5, b = its)

sc1 <- ass_scatter_lm(lt_data = chk_1, mains = "1 year", p_type = "s")
sc2 <- ass_scatter_lm(lt_data = chk_3, mains = "3-mean years", p_type = "m")
sc3 <- ass_scatter_lm(lt_data = chk_5, mains = "5-mean years", p_type = "m")

combi1 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "r2", x_axis = "R²") %>% 
  ggpar(xlim = c(0,1))
combi2 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "residuals", x_axis = "GLM response residuals", mode = TRUE)
combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "density", x_axis = "Chl-a, kernel density\nhigh- (50, filled) &\nlow-bandwith (10, lines)", vert_line = NULL) # %>% ggpar(xlim = c(0,1))

dragon <- (sc3 + combi1) / (sc2 + combi2) / (sc1 + combi3) + plot_layout(guides = "collect")
dragon


ggsave(filename = "proxy AUS.svg", plot = dragon, height = hgt, width = wd)

##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## 3. Scenario few years with different response in same lake, stable alternative states (all years with alternative states follow each other) -----------
# See 1. SI scenario for more comments, especially on the plotting and plot export
lakes <- 99 # number of lakes
samples <- 20 # lake years

alternative_state <- sample(runif(lakes, 0.2, 0.8)) # share of time series with different alternative stable states, here between 20% and 80%

# set mean x (nutrient concentration) for each lake, based on mean range in data (6-year means)
set.seed(2001) # a space odyssey
seqs <- sample(seq(0.33, 4.93, length.out = lakes)) # random sample for 20 lakes based on normal distribution for mean range based on true range and cv * mean, as cv is stable over data mean range, with reduced SD uncertainty, as a higher SD would result in x values partially out of the range of the real world data, abs = absolute values to 

y_shape <- 15

lake <- NULL; y <- NULL; x <- NULL; type <- NULL
for(i in 1:lakes) {
  sl_a <- 120#*5 # rnorm(20, 30, y_sd)
  sl_b <- 40#*5 #rnorm(20, 30, y_sd) # alternative
  
  ic_a <- 50 #rnorm(20, 250, y_sd)
  ic_b <- 0 #rnorm(20, 50, y_sd) # alternative
  
  set.seed(i+1984)  # George Orwell
  s <- sample.int(round(samples * alternative_state[i],0))
  
  set.seed(i+1984)  # George Orwell
  loop_x <- abs(rnorm(n = samples, mean = seqs[i], sd = seqs[i]*0.35)) # random sample for 20 lakes based on normal distribution for mean range based on true range and cv * mean, as cv is stable over data mean range, with reduced SD uncertainty, as a higher SD would result in x values partially out of the range of the real world data, abs = absolute values to 
  
  x_a <- loop_x[-s]
  x_b <- loop_x[s]
  
  y_a_t <- sl_a * loop_x[-s] + ic_a
  y_b_t <- sl_b * loop_x[s] + ic_b 
  
  set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
  y <- c(y, rgamma(samples - round(samples * alternative_state[i], 0), rate = y_shape / y_a_t, shape = y_shape))
  set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
  y <- c(y, rgamma(round(samples * alternative_state[i], 0), rate = y_shape / y_b_t, shape = y_shape))
  
  # Combine data
  x <- c(x,
         c(x_a, x_b)
  )
  
  type <- c(type, 
            c(rep("a", length(x_a)), rep("b", length(x_b)))
  )
  
  lake <- c(lake, 
            rep(i,length(c(x_a, x_b)))
  )
}

sim_data <- data.frame(lake, type, x, y)

sim_data %>% ggscatter(y = "y", x = "x", color = "type", main = "Full simulated data")

chk_1 <- hier_resampl(n_means = 1, b = its)
chk_3 <- hier_resampl(n_means = 3, b = its)
chk_5 <- hier_resampl(n_means = 5, b = its)

sc1 <- ass_scatter_lm(lt_data = chk_1, mains = "1 year", p_type = "s")
sc2 <- ass_scatter_lm(lt_data = chk_3, mains = "3-mean years", p_type = "m")
sc3 <- ass_scatter_lm(lt_data = chk_5, mains = "5-mean years", p_type = "m")

combi1 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "r2", x_axis = "R²") %>% 
  ggpar(xlim = c(0,1))
combi2 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "residuals", x_axis = "GLM response residuals", mode = TRUE)
combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "density", x_axis = "Chl-a, kernel density\nhigh- (50, filled) &\nlow-bandwith (10, lines)", vert_line = NULL) # %>% ggpar(xlim = c(0,1))

dragon <- (sc3 + combi1) / (sc2 + combi2) / (sc1 + combi3) + plot_layout(guides = "collect")
dragon

ggsave(filename = "proxy ASS.svg", plot = dragon, height = hgt, width = wd)



##~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
## 4. Scenario few years with different response in different lakes-------------------------
# See 1. SI scenario for more comments, especially on the plotting and plot export

# set number of lakes 
lakes <- 99  # number of lakes, based on number of lakes in data (5-year means)
samples <- 20 # number lake years min to max, based on range of sampled years per lake in data (6-year means)


# set mean x (nutrient concentration) for each lake, based on mean range in data (6-year means)
set.seed(2001) # a space odyssey
seqs <- sample(seq(0.33, 4.93, length.out = lakes)) # mean range based on total nitrogen range in data (with higher shape variable (below), this also works for total phosphorus)

# Variability of the Gamma distribution, with a shape of 15 for each of the different state populations, one gets a simulation output of approx 2.63 for the entire simulation dataset, which is close to the real data (true value is 2.63 ± 0.08 SE (GLM with TN+TP as explanatory variables))
y_shape <- 15

set.seed(2001) # a space odyssey
seqs <- sample(seq(0.33, 4.93, length.out = lakes))


alternative_state <- seq(2, 100, by = 2) #each second lake has a different response, this is used in the SI
#for playing around:
#alternative_state <- seq(5, 50, by = 5) #each fifth lake has a different response


lake <- NULL; y <- NULL; x <- NULL; type <- NULL
for(i in 1:lakes) {
  # first state
  if(any(alternative_state == i)) {
    sl <- 40 # rnorm(20, 30, y_sd)
    ic <- 0 #rnorm(20, 250, y_sd)
    
    #s <- sample.int(samples) # shuffling of years
    set.seed(i+1984)  # George Orwell
    loop_x <- abs(rnorm(n = samples, mean = seqs[i], sd = seqs[i]*0.35)) 
    
    yt <- sl * loop_x + ic
    
    y <- c(y, 
           rgamma(samples, rate = y_shape / yt, shape = y_shape)
    )
    
    x <- c(x, loop_x)
    
    type <- c(type, 
              c(rep("b", length(loop_x)))
    )
    
    lake <- c(lake, 
              rep(i,length(loop_x))
    )
  } else { # second state
    
    sl <- 120#*5 # rnorm(20, 30, y_sd)
    ic <- 50 #rnorm(20, 250, y_sd)
  
    set.seed(i+1984)  # George Orwell
    loop_x <- abs(rnorm(n = samples, mean = seqs[i], sd = seqs[i]*0.35)) 
    
    yt <- sl * loop_x + ic
    
    y <- c(y, 
           rgamma(samples, rate = y_shape / yt, shape = y_shape)
    )
    
    x <- c(x, loop_x)
    
    type <- c(type, 
              c(rep("a", length(loop_x)))
      )
    
    lake <- c(lake, 
              rep(i,length(loop_x))
    )
  }
}

sim_data <- data.frame(lake, type, x, y)

sim_data %>% ggscatter(y = "y", x = "x", color = "type", main = "Full simulated data")

chk_1 <- hier_resampl(n_means = 1, b = its)
chk_3 <- hier_resampl(n_means = 3, b = its)
chk_5 <- hier_resampl(n_means = 5, b = its)

sc1 <- ass_scatter_lm(lt_data = chk_1, mains = "1 year", p_type = "s")
sc2 <- ass_scatter_lm(lt_data = chk_3, mains = "3-mean years", p_type = "m")
sc3 <- ass_scatter_lm(lt_data = chk_5, mains = "5-mean years", p_type = "m")

combi1 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "r2", x_axis = "R²") %>% 
  ggpar(xlim = c(0,1))
combi2 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "residuals", x_axis = "GLM response residuals", mode = TRUE)
combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "density", x_axis = "Chl-a, kernel density\nhigh- (50, filled) &\nlow-bandwith (10, lines)", vert_line = NULL) # %>% ggpar(xlim = c(0,1))

dragon <- (sc3 + combi1) / (sc2 + combi2) / (sc1 + combi3) + plot_layout(guides = "collect")
dragon


ggsave(filename = "proxy ASS_difflakes.svg", plot = dragon, height = hgt, width = wd)

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Main text simulation, alternative stable states with random appearance in time series (all years with alternative states follow each other) of one lake (1/3 of data) or in different lake time series (1/3 of data in state a, 1/3 of data in state b) -----
# See 1. SI scenario for more comments, especially on the plotting and plot export
# in contrast to SI time series, this one contains simulated time series with variable length

# set number of lakes 
lakes <- 99 # number of lakes, based on number of lakes in data (6-year means)

# set number of sampled years, which is based on time series length in data
set.seed(2001)  # a space odyssey

# in contrast to SI time series, this one contains simulated time series with variable length
samples <- round(runif(lakes, min = 5, max = 20),0) # number lake years min to max, based on range of sampled years per lake in data (6-year means)

# set mean x (nutrient concentration) for each lake, based on mean range in data (6-year means)
set.seed(2001) # a space odyssey
seqs <- sample(seq(0.33, 4.93, length.out = lakes)) # mean range based on total nitrogen range in data (with higher shape variable (below), this also works for total phosphorus)

# Assumption on appearance of alternative states, mix of scenarios containing times at which alternative stable states may appear
# one third of lakes in state a
# one third of lakes in state b
# one third of lakes partially in state a or b, hence these shift from one state to the other during the simulated time series
set.seed(2001) # a space odyssey
low_ass <- sample(rep(0, 1000), lakes / 3) # state a
set.seed(2001) # a space odyssey
high_ass <- sample(rep(1, 1000), lakes / 3) # state b
set.seed(2001) # a space odyssey
within_ts <- sample(runif(1000, 0.2, 0.8), lakes / 3) # share of lakes with alternative stable states (partially a (20-80% of time) and b (remaining time))
alternative_state <- c(within_ts, low_ass, high_ass)


y_shape <- 15

# Loop to create simulated dataset ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~+
lake <- NULL; y <- NULL; x <- NULL; type <- NULL
for(i in 1:lakes) {

# Population in state a
sl_a <- 120 # slope
ic_a <- 50 # intercept

# Population in state b
sl_b <- 40 # slope
ic_b <- 0 # intercept

# for lakes in state a
if(round(samples[i] * alternative_state[i],0) == 0) {
  
  set.seed(i+1984)  # George Orwell
  loop_x <- abs(rnorm(n = samples[i], mean = seqs[i], sd = seqs[i]*0.35)) # mean range based on true range and cv * mean, as cv is stable over data mean range
  # loop_x <- abs(rnorm(samples[i], mean = 0.35, sd = 0.06)) # mean and sd based on avg ptot lake mean and sd
  
  x_b <- loop_x
  y_b_t <- sl_b * loop_x + ic_b 
  
  set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
  y <- c(y, rgamma(samples[i], rate = y_shape / y_b_t, shape = y_shape)
  )
  
  x <- c(x, x_b)
  
  type <- c(type, rep("b", length(x_b)))
  
  lake <- c(lake, rep(i,length(x_b)))
} else { # for lakes in state b
  
  if(round(samples[i] * alternative_state[i],0) == samples[i]) {
    
    set.seed(i+1984)  # George Orwell
    loop_x <- abs(rnorm(n = samples[i], mean = seqs[i], sd = seqs[i]*0.35)) # mean range based on true range and cv * mean, as cv is stable over data mean range
    
    
    x_a <- loop_x
    y_a_t <- sl_a * loop_x + ic_a 
    
    set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
    y <- c(y, rgamma(samples[i], rate = y_shape / y_a_t, shape = y_shape)
    )
    
    x <- c(x, x_a)
    
    type <- c(type, rep("a", length(x_a)))
    
    lake <- c(lake, rep(i,length(x_a)))
    
  } else { # for lakes with alternative stable states within the time series
    s <- sample.int(round(samples[i] * alternative_state[i],0))
    
    set.seed(i+1984)  # George Orwell
    loop_x <- abs(rnorm(n = samples[i], mean = seqs[i], sd = seqs[i]*0.35)) # mean range based on true range and cv * mean, as cv is stable over data mean range
    
    x_a <- loop_x[-s]
    x_b <- loop_x[s]
    
    y_a_t <- sl_a * loop_x[-s] + ic_a
    y_b_t <- sl_b * loop_x[s] + ic_b 
    
    set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
    y <- c(y, rgamma(samples[i] - round(samples[i] * alternative_state[i], 0), rate = y_shape / y_a_t, shape = y_shape))
    set.seed(i+42) # Don’t Panic. “What is the meaning of life, the universe, and everything?”
    y <- c(y, rgamma(round(samples[i] * alternative_state[i], 0), rate = y_shape / y_b_t, shape = y_shape))
    
    
    x <- c(x,
           c(x_a, x_b)
    )
    
    type <- c(type, 
              c(rep("a", length(x_a)), rep("b", length(x_b)))
    )
    
    lake <- c(lake, 
              rep(i,length(c(x_a, x_b)))
    )
  }
}
}

# Construction of dataset
sim_data <- data.frame(lake, type, x, y)

# use shape paramater to get close to distribution shape parameter of 2.63 (which has been estimated from the true data)
gamma.shape(glm(y ~ x, data = sim_data, family = Gamma(link = "identity")))

# Simulation data distribution, with belonging to each alternative state color-coded in the plot (for the sake of the reader), belonging to alternative stable state is unknown to the resampling procedure!
sim_data %>% ggscatter(y = "y", x = "x",  main = "Full simulated data", color = "type")

# Resampling for 1, 3 and 5 mean years ~~~~~~~~~~~~~~~~~~~~
chk_1 <- hier_resampl(n_means = 1, b = its)
chk_3 <- hier_resampl(n_means = 3, b = its)
chk_5 <- hier_resampl(n_means = 5, b = its)

# Plots ~~~~~~~~~~~~~~~~~~
sc1 <- ass_scatter_lm(lt_data = chk_1, mains = "1 year", p_type = "s")
sc2 <- ass_scatter_lm(lt_data = chk_3, mains = "3-mean years", p_type = "m")
sc3 <- ass_scatter_lm(lt_data = chk_5, mains = "5-mean years", p_type = "m")

combi1 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "r2", x_axis = "R²") %>% 
  ggpar(xlim = c(0,1))
combi2 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "residuals", x_axis = "GLM response residuals", mode = TRUE)
# HD p value plot, not used in revised paper
# combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "HD", x_axis = "Hartigans' dip test p-value", vert_line = 0.05) %>%
#   ggpar(xlim = c(0,1))

combi3 <- combi_sim_plot(m1 = chk_1, m3_m = chk_3, m7_m = chk_5, modeltype = "y_x", variable = "density", x_axis = "y kernel density\nhigh- (50, filled) & low-bandwith (10, lines)")

# Combination of plots
dragon <- (sc3 + combi1) / (sc2 + combi2) / (sc1 + combi3) + plot_layout(guides = "collect")
dragon

ggsave(filename = "Simulation_based on real data dist_with common ASS.svg", plot = dragon, height = 7, width = 5)
