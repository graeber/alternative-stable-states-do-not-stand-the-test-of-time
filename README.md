# Readme on the code and further supplementary information for "Bimodality and alternative equilibria do not help explain long-term patterns in shallow lake chlorophyll-a"

## Research paper
The information of this repository is used in the following publication:  
Davidson, T.A., Sayer, C.D., Jeppesen, E., Søndergaard, M., Lauridsen, T.L., Johansson, L.S., Baker, A., Graeber, D., 2023. Bimodality and alternative equilibria do not help explain long-term patterns in shallow lake chlorophyll-a. Nat Commun 14, 398. https://doi.org/10.1038/s41467-023-36043-9

## Used data sources

1. The [LAGOS-NE](https://lagoslakes.org/products/data-products/) database contains data from the North-Eastern USA. Data is open-access. Information on the dataset can be found in [Soranno et al. (2017)](https://academic.oup.com/gigascience/article/6/12/gix101/4555226?login=false). The data can be accessed through an R library, called [LAGOSNE](https://cran.r-project.org/web/packages/LAGOSNE/)
2. The [Overfladevandsdatabasen (surface water database)](https://odaforalle.au.dk/login.aspx) contains data from Denmark. Data is open-access. The conditions for using this data can be found on the website of the Overfladevandsdatabasen.

The data used in the paper was downloaded from these two databases. To allow for full replication of our data analysis, the downloaded data used in the paper is linked here.
1. [LAGOS-NE downloaded data](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/Data/LAGOSNE_2022-02-22_prep.csv/)
1. [Overfladevandsdatabasen downloaded data](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/Data/big_ass_data_2.csv/)

*If using this downloaded data in a publication or elsewhere, please cite the original data source mentioned above.* 

## R code

### R version and R packages

R versions 4.1 and above were used in the data analysis.

The R code within this repository only uses the following R libraries, all available in [CRAN](https://cran.r-project.org):

1. [tidyverse](https://cran.r-project.org/web/packages/tidyverse/)
2. [foreach](https://cran.r-project.org/web/packages/foreach)
3. [doParallel](https://cran.r-project.org/web/packages/doParallel/)
4. [diptest](https://cran.r-project.org/web/packages/diptest/)
5. [ggpubr](https://cran.r-project.org/web/packages/ggpubr/)
6. [patchwork](https://cran.r-project.org/web/packages/patchwor/)
7. [LAGOSNE](https://cran.r-project.org/web/packages/LAGOSNE/)

### R code files

This repository contains four R code files. Below, there is a short description of what the code does, see methods section of the linked paper for details on the data preparation and analysis steps:

1. [Data preparation code for LAGOS-NE data](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/data-preparation_lagosne.R/). Calculates growing period averages, filters for shallow lakes, and removes lakes with extremely high TN and TP concentrations. Also detects continuity of time series (hence, whether there are data gaps) for mean calculation later.
2. [Data preparation code for data from Overfladevandsdatabasen](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/data-preparation_lagosne.R/). Filters for shallow lakes, and removes lakes with extremely high TN and TP concentrations. Moreover, removes brackish waters, which were outside of the scope of this study. Also detects continuity of time series (hence, whether there are data gaps) for mean calculation later.
3. [Code to combine the two datasets, calculate moving averages from growing season means, and calculate the hierarchical Bootstrap](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/data-combination_mean-calculation_bootstrap-calculation.R/). This file contains all data analyses steps shown in the paper for real data.
4. [Simulation code](https://git.ufz.de/-/ide/project/graeber/alternative-stable-states-do-not-stand-the-test-of-time/tree/main/-/simulation-scenarios_main-text_SI.R/). The code presented here was used for the simulations in the main manuscript, and in the supplementary material.



## Contact for questions
Please contact **Daniel Graeber** (daniel.graeber[at]ufz.de) or **Thomas A. Davidson** (thd[at]ecos.au.dk) if you have questions

## License
This project is licensed under the BSD 3-Clause "New" or "Revised" License.

